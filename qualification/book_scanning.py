#!/bin/pypy3

import sys
from copy import copy
from os.path import join, basename

Available_days = 0
Book_scores = []
Scanned_books = set()
Available_books = set()

class Library:
    def __init__(self, libid, books_total, signup_time, books_per_day, books_list):
        self.libid = libid
        self.books_total = books_total
        self.signup_time = signup_time
        self.books_per_day = books_per_day
        self.books_list = books_list
        self.books_with_scores = list(map(lambda i: (i, Book_scores[i]), self.books_list))
        self.books_with_scores.sort(key=lambda x: x[1], reverse=True)
        self.books_given = []

    def __str__(self):
        return str(self.libid) + '(' + str(self.books_total) + ', ' + str(self.signup_time) + ', ' + str(self.books_per_day) + '): ' + str(self.books_with_scores)

    def total_score(self):
        # Clean books score
        Scanned_books_max = self.books_per_day*(Available_days - self.signup_time)
        max_score = 0
        books_scanned = 0
        for book_with_score in self.books_with_scores:
            if book_with_score[0] not in Scanned_books and book_with_score[0] not in Available_books:
                max_score += book_with_score[1]
                books_scanned += 1
            elif book_with_score[0] in Scanned_books:
                self.books_with_scores.remove(book_with_score)

            if books_scanned == Scanned_books_max:
                break

        return max_score / self.signup_time

    # Return a list of books_per_day books that haven't been scanned or an empty list
    # if no such books exist
    def get_books(self):
        ret = []
        books_scanned = 0
        while len(self.books_with_scores) > 0 and books_scanned < self.books_per_day:
            tmp = self.books_with_scores.pop(0)
            if tmp[0] not in Scanned_books:
                self.books_given.append(tmp[0])
                ret.append(tmp)
                books_scanned += 1
        return ret

def main(inputfilename, outputfilename):
    inputfile = open(inputfilename)
    input_lines = inputfile.readlines()

    books_count, libraries_count, scan_days = input_lines[0].split()
    books_count = int(books_count)
    libraries_count = int(libraries_count)
    scan_days = int(scan_days)
    # print('scan_days = ' + str(scan_days))

    global Available_days
    Available_days = scan_days
    global Scanned_books
    Scanned_books = set()

    global Book_scores
    Book_scores = [int(x) for x in input_lines[1].split()]

    libraries = []
    libraries_signed = []

    # Create a set of all available books in the signed up libraries
    global Available_books
    Available_books = set()

    libid = 0
    for i in range(2, len(input_lines) - 1, 2):
        library = [int(x) for x in input_lines[i].split()]
        library_books = [int(x) for x in input_lines[i + 1].split()]
        libraries.append(Library(libid, library[0], library[1], library[2], library_books))
        libid += 1

    libraries.sort(reverse=False, key=lambda x: x.signup_time)
    library_in_signup = None
    library_in_signup_days = 0
    library_signup_order = []

    score = 0
    K = 1000
    for day in range(0, scan_days):
        # K = int(0.2 * len(libraries)) + 1
        # print('day = ' + str(day))
        Available_days -= 1
        # Signup new libraries
        if library_in_signup is None and len(libraries) > 0:
            # Signup the best library among the K first in the libraries
            next_K_scores = list(map(lambda l: (l, l.total_score()), libraries[:K]))
            library_in_signup_with_score = max(next_K_scores, key=lambda l: l[1])
            library_in_signup = library_in_signup_with_score[0]
            next_K_scores.remove(library_in_signup_with_score)
            libraries.remove(library_in_signup)
        
        library_in_signup_days += 1

        if library_in_signup is not None and library_in_signup_days == library_in_signup.signup_time:
            libraries_signed.append(library_in_signup)
            library_signup_order.append(library_in_signup)
            Available_books = Available_books.union(set(library_in_signup.books_list))
            library_in_signup = None
            library_in_signup_days = 0

        # Get them books!
        for library in libraries_signed:
            new_books = library.get_books()
            if len(new_books) == 0: # If the library has run out of useful books
                libraries_signed.remove(library)
                break
            for book in new_books:
                score += book[1]
                Scanned_books.add(book[0])
                Available_books.remove(book[0])

    print(score)

    library_count = 0
    for library in library_signup_order:
        if len(library.books_given) > 0:
            library_count += 1

    output_lines = [str(library_count) + '\n']
    for library in library_signup_order:
        if len(library.books_given) > 0:
            output_lines.append(str(library.libid) + ' ' + str(len(library.books_given)) + '\n')
            books = [str(x) for x in library.books_given]
            output_lines.append(' '.join(books) + '\n')

    # print(output_lines)
    with open(outputfilename, 'w') as outputfile:
        outputfile.writelines(output_lines)

    # for id, library in libraries.items():
    #     print(id)
    #     print(library)
    #     print(library.total_score())

if __name__ == "__main__":
    output_folder = sys.argv[1]
    input_files = sys.argv[2:]
    for f in input_files:
        main(f, join(output_folder, basename(f) + '.out'))
