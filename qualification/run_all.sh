#!/bin/bash

out_folder="outputs"

if [ "$1" != "" ]; then
    out_folder="$1"
fi

./book_scanning.py $out_folder input/a_example.txt input/b_read_on.txt input/c_incunabula.txt input/d_tough_choices.txt input/e_so_many_books.txt input/f_libraries_of_the_world.txt