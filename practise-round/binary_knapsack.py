#!/bin/python3
"""
A Dynamic Programming based Python Program for 0-1 Knapsack problem
Returns the maximum value that can be put in a knapsack of capacity W

@author Bhavya Jain (https://www.geeksforgeeks.org/0-1-knapsack-problem-dp-10/)
@author Michalis Vardoulakis
"""

import sys
import gc

def knapSack(M, wt, N):
    # FIXME Further reduce memory footprint by using Numpy arrays wherever
    # possible
    K_prev_weight = [0 for x in range(M+1)]
    K_prev_selections = [[] for x in range(M+1)]
    K_cur_weight = [0 for x in range(M+1)]
    K_cur_selections = [[] for x in range(M+1)]

    # Build table K[][] in bottom up manner, only keeping current and previous
    # and current lines of K 
    for i in range(N+1):
        for w in range(M+1):
            if i==0 or w==0:
                continue
            elif wt[i-1] <= w:
                if wt[i-1] + K_prev_weight[w-wt[i-1]] > K_prev_weight[w]:
                    K_cur_weight[w] = K_prev_weight[w-wt[i-1]] + wt[i-1]
                    K_cur_selections[w] = K_prev_selections[w-wt[i-1]] + [i-1]
                else:
                    K_cur_weight[w] = K_prev_weight[w]
                    K_cur_selections[w] = K_prev_selections[w]
            else: 
                K_cur_weight[w] = K_prev_weight[w]
                K_cur_selections[w] = K_prev_selections[w]
        K_prev_weight = K_cur_weight
        K_prev_selections = K_cur_selections
        K_cur_weight = [0 for x in range(M+1)]
        K_cur_selections = [[] for x in range(M+1)]
        gc.collect()
    # print(K)
    return (K_prev_weight[M], K_prev_selections[M])

# Driver program to test above function
def main():
    # FIXME Output needs to be indexes of items in the knapsack

    infilename = sys.argv[1]
    infile = open (infilename, 'r')
    inlines = infile.readlines()
    M = int(inlines[0].split(' ')[0])
    wt = [int(x) for x in inlines[1].split()]

    N = len(wt)
    result = knapSack(M, wt, N)
    print(result[0], len(result[1]))
    print(' '.join([str(x) for x in result[1]]))

if __name__ == '__main__':
    main()
