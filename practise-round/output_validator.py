#!/bin/python3

import sys

def main():
    infilename = sys.argv[1]
    infile = open(infilename, 'r')
    outfilename = sys.argv[2]
    outfile = open(outfilename, 'r')

    M = int(infile.readline().split()[0])
    wt = [int(x) for x in infile.readline().split()]
    selected_total_weight = int(outfile.readline())
    selected_count = int(outfile.readline())
    selected = [int(x) for x in outfile.readline().split()]

    sum_val = sum([wt[i] for i in selected])
    if selected_total_weight == sum_val:
        print('PASS')
    else:
        print('FAIL %d != %d' % (selected_total_weight, sum_val))




if __name__ == "__main__":
    main()