#!/bin/python3

import itertools
import sys

def knapSack(M, wt):
    weight = 0
    selected = set()

    for i in range(len(wt)):
        assert(i not in selected)
        if weight + wt[i] <= M: # If space left to add it then do and move on
            selected.add(i)
            weight += wt[i]
            if weight == M:
              return (weight, selected)
            continue

        # If run out of space then examine if removing any subset of the
        # selected elements to add the new one results in a better solution
        next_weight = weight
        next_selected = selected
        for j in range(1, len(selected) + 1):
            for subset in itertools.combinations(selected, j):
                assert(i not in selected)
                subset_weight = sum(wt[x] for x in subset)
                if weight >= weight - subset_weight + wt[i]:
                    break
                if next_weight < weight - subset_weight + wt[i] <= M:
                    next_selected = selected - set(subset)
                    next_selected.add(i)
                    if not len(next_selected) == len(selected) - len(subset) + 1:
                        import pdb; pdb.set_trace()
                    next_weight = weight - subset_weight + wt[i]
                    if next_weight == M:
                        return (next_weight, next_selected)
        selected = next_selected
        weight = next_weight

    return (weight, selected)


def main():
    infilename = sys.argv[1]
    infile = open (infilename, 'r')
    inlines = infile.readlines()
    M = int(inlines[0].split(' ')[0])
    wt = [int(x) for x in inlines[1].split()]

    result = knapSack(M, wt)
    print(result[0]) # !!! Comment out for expected output/leave to use run-and-validate.sh
    print(len(result[1]))
    print(' '.join([str(x) for x in result[1]]))

if __name__ == '__main__':
    main()
