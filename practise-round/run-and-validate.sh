#!/bin/bash

INPUT_FOLDER=input-data
OUTPUT_FOLDER=output-data

for infile in $(ls $INPUT_FOLDER); do
    outfile="$(basename "$infile" .in).out"
    ./iterative_knapsack.py $INPUT_FOLDER/$infile > $OUTPUT_FOLDER/$outfile
    ./output_validator.py $INPUT_FOLDER/$infile $OUTPUT_FOLDER/$outfile
done